const express       = require('express');
const app           = express();
const mongooes      = require('mongoose');
const jwt           = require('jsonwebtoken');
const morgan        = require('morgan');
const cookieParser  = require('cookie-parser');
const bodyParser    = require('body-parser');
const configDB      = require('./config/database.js');
const port          = process.env.PORT || 8080;

// mongodb configuration 
mongooes.connect(configDB.url,configDB.options);

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
// get information from html forms
app.use(bodyParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('public'));

app.set('view engine', 'ejs'); // set up ejs for templating
app.set('superSecret',configDB.secret);
app.set('jwtoken',jwt);

// routes 
require('./app/routes.js')(app); // load our routes

// launch 
app.listen(port);
console.log('Server is running on port:' + port);