var User=require('./../models/user');
const configDB = require('../../config/database.js');
const jwt      = require('jsonwebtoken');

var authenticate=function(req,res,next){
    var token= req.cookies.auth;
    var decoded;

    if(token){
        decoded=jwt.verify(token,configDB.secret);
        User.findById(decoded._id,function(err,user){
            if (err) throw err;
            req.body.isLoggedIn=true;
            next();
        });      
    }
    else
    {
        if(req.url=='/login' || req.url=='/signup'){
            req.body.isLoggedIn=false;
            next();
        }
        else{
            res.sendStatus(401);
        }
    }
    
}

module.exports=authenticate; 