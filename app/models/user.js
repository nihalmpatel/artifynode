// app/models/user.js

const mongoose = require('mongoose');
const jwt      = require('jsonwebtoken');
const configDB = require('../../config/database.js');
const _        = require('lodash'); 

// user Model ===========================================
var userSchema = mongoose.Schema({

    local           : {
        username    : {type:String, unique:true, required: true} ,
        password    : {type:String, required: true},
        email       : {type:String, unique:true, required: true} ,
    },

    profile         : {
        fullname    : String,
        profilepic  : String,
        bio         : String,
        age         : Number,
        location    : String,
    },

    following       : [],
    followers       : [],
    stories         : [],
    interests       : [],
    tokens          : [{access:String,token:String}]

    /*facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    } */

});

// =====================================================

// methods =============================================

userSchema.methods.generateToken = function(access){
    var user=this;
    var token=jwt.sign({_id:user._id.toHexString(),access},configDB.secret);
    if(access=="changepass"){
        user.tokens.push({access:access,token:token});
    }
    user.save();
    return token;
}

/*
userSchema.methods.removeAuthToken=function(token){
    var user=this;
    if(access=="changepass"){
        var i=user.tokens.indexOf({token:token});
        //_.remove(user.tokens,{token:token});
        user.tokens.pop();
    }
    user.save();
}*/


userSchema.methods.toJSON = function() {
    var user=this;
    var userObject=user.toObject();
    return _.pick(userObject,['_id','local.username','profile.fullname']);
}

// =======================================================

// create the model for users and expose it to our app
module.exports = mongoose.model('user', userSchema);