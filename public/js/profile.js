// username of the profile mentioned in url
var username;
var totalfollowers;
$(document).ready(function() {
    // Save Profile
    $('#saveprofile').click(function(e) { 
        var fullname=$('#fullname').val();
        var bio=$('#bio').val();
        var location=$('#location').val();
        var age=$('#age').val();
        console.log(fullname);
        console.log(bio);
        console.log(location);
        console.log(age);
        if(!fullname.trim() || !bio.trim() || !location.trim()){
            alert('Inputs should not be empty or shouldn\'t contain only whitespaces!');
        }

        if(age<6 || age>110){
            alert('Enter valid age!');
        }
        else{
            var data = {};
            data.fullname = fullname;
            data.bio = bio;
            data.location= location;
            data.age=age;
            
            e.preventDefault();
                $.ajax({
                        type: "POST",
                        url: "/editprofile",
                        data: data,
                        success: function(data) {
                            //show content
                            window.location = "/@"+username;
                        },
                        error: function(jqXHR, textStatus, err) {
                            //show error message
                            alert('text status '+textStatus+', err '+err);
                        } 
                });
        }
    });

    // Edit Profile
    $('#editprofile').click(function(e) {
        window.location="/editprofile";
    });

    // Follow/Unfollow
    $('button.followButton').on('click', function(e){
        data={}
        data.followedUsername= username;
        e.preventDefault();
        $button = $(this);

        if($button.hasClass('btn-success')){
            
            $.ajax({
                type: "POST",
                url: "/follow",
                data: data,
                success: function(data) {
                    
                },
                error: function(jqXHR, textStatus, err) {
                    //show error message
                    alert('error occured:'+err);
                } 
            });
            
            $button.removeClass('btn-success').addClass('btn-danger');
            $button.text('Unfollow');
            totalfollowers=parseInt(totalfollowers)+1;
            $('#followers').html("Followers: "+totalfollowers.toString());
        } 
        
        else{
            
            $.ajax({
                type: "POST",
                url: "/unfollow",
                data: data,
                success: function(data) {

                },
                error: function(jqXHR, textStatus, err) {
                    //show error message
                    alert('error occured:'+err);
                } 
            });
         
            $button.removeClass('btn-danger').addClass('btn-success');
            $button.text('Follow');
            totalfollowers=parseInt(totalfollowers)-1;
            $('#followers').html("Followers: "+totalfollowers.toString());
        }
        
    });

    /*
    // Follow
    $('#follow').click(function(e) {
        data={}
        data.followedUsername= username;
        e.preventDefault();
            $.ajax({
                type: "POST",
                url: "/follow",
                data: data,
                success: function(data) {
                    
                },
                error: function(jqXHR, textStatus, err) {
                    //show error message
                    alert('error occured:'+err);
                } 
            });
            $('#follow').attr("class","btn btn-danger");
            $('#follow').html("Unfollow");
            $('#follow').attr("id","unfollow");
            $('#followers').html("Followers: "+totalfollowers+1);
    });

    // Unfollow
    $('#unfollow').click(function(e) {
        data={}
        data.followedUsername= username;
        e.preventDefault();
            $.ajax({
                type: "POST",
                url: "/unfollow",
                data: data,
                success: function(data) {

                },
                error: function(jqXHR, textStatus, err) {
                    //show error message
                    alert('error occured:'+err);
                } 
            });
            $('#unfollow').attr("class","btn btn-success");
                    $('#unfollow').html("Follow");
                    $('#unfollow').attr("id","follow");
    });*/

});