function signupvalidation(){
    var username=$('#username').val();
    var email=$('#email').val();
    var pass=$('#pass').val();
    var cpass=$('#cpass').val();

    if(!username.trim() || !email.trim() || !pass.trim() || !cpass.trim()){
        alert('Input should not be empty or contains white spaces!');
        return false;
    }

    if(pass.length<6 || cpass.length<6){
        alert('Password must have minimum length of 6!');
        return false;
    }
    if(pass!==cpass){
        alert('Password and Confirm Password should match!');
        return false;
    }
}